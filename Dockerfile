FROM node:10-alpine
WORKDIR /usr/src/app

COPY . .
# Set timezone
ENV TZ=Asia/Bangkok
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone.

RUN npm install
RUN npm run build

EXPOSE 4000
CMD [ "npm", "run", "start:prod"]