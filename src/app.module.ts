import { Module } from '@nestjs/common';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ServerModule } from './server/server.module';
import { ServerHistoryModule } from './server-history/server-history.module';
import { ServicesModule } from './services/services.module';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { APP_FILTER } from '@nestjs/core';
import { HttpErrorFilter } from './shared/http-error.filter';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService): TypeOrmModuleOptions => ({
        type: 'mysql',
        host: configService.get<string>('TYPEORM_HOST', 'localhost'),
        port: configService.get<number>('TYPEORM_PORT', 3306),
        username: configService.get<string>('TYPEORM_USERNAME', 'username'),
        password: configService.get<string>('TYPEORM_PASSWORD', 'password'),
        database: configService.get<string>('TYPEORM_DATABASE', 'database'),
        synchronize: true,
        entities: ['./dist/**/*.entity{.ts,.js}'],
        logging: true,
      })
    }),
    ServerModule,
    ServerHistoryModule,
    ServicesModule,
    UserModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_FILTER,
      useClass: HttpErrorFilter,
    },
  ],
})
export class AppModule { }
