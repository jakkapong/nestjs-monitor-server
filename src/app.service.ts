import { Injectable, OnModuleInit } from '@nestjs/common';
import { ServerService } from './server/server.service';
import { ServicesService } from './services/services.service';

@Injectable()
export class AppService implements OnModuleInit {

  constructor(
    private readonly serverService: ServerService,
    private readonly serviceService: ServicesService,
  ) { }

  getRoot(): any {
    return {
      status: 'ok',
      message: 'Monitor Server'
    };
  }

  async onModuleInit() {
    try {
      const servers = await this.serverService.initializeLoadServer();
      if (!servers) { return false }
      for (const iterator of servers) {
        await this.serviceService.pingStartByServer(iterator);
      }
      return true;
    } catch (error) {
      return false;
    }
  }
}
