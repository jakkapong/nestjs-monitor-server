import { Body, Controller, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { LoginDto } from './auth.model';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {

  constructor(
    private readonly service: AuthService,
  ) { }

  @Post('/login')
  @UsePipes(ValidationPipe)
  login(@Body() dto: LoginDto) {
    return this.service.login(dto)
  }
}
