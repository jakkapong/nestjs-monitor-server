import { Injectable, Logger } from '@nestjs/common';
import { LoginDto } from './auth.model';

@Injectable()
export class AuthService {
  private readonly logger = new Logger('AuthService')

  constructor() { }

  login(dto: LoginDto) {
    return {
      route: '/auth/login',
      ...dto,
    }
  }
}
