import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);

    // Set Cors
    app.enableCors();

    // Set Environment
    const configService: ConfigService = app.get(ConfigService);
    const port = parseInt(configService.get('PORT')) || 4000;
    const nodeEnv = configService.get('NODE_ENV') || 'development';

    // Set Swagger
    const options = new DocumentBuilder()
        .setTitle('Monitor Server Api Service')
        .setDescription('Monitor Server Api description')
        .setVersion('1.0')
        .build();
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('document', app, document);

    await app.listen(port);
    Logger.log(`Server ${nodeEnv} running on http://localhost:${port}`, 'Bootstrap');
}
bootstrap();
