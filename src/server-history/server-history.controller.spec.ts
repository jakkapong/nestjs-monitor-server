import { Test, TestingModule } from '@nestjs/testing';
import { ServerHistoryController } from './server-history.controller';

describe('ServerHistory Controller', () => {
  let controller: ServerHistoryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ServerHistoryController],
    }).compile();

    controller = module.get<ServerHistoryController>(ServerHistoryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
