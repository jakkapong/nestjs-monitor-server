import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne } from "typeorm";
import { ServerStatus } from "./server-history.interface";
import { ServerEntity } from "src/server/server.entity";

@Entity('server_history')
export class ServerHistoryEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("enum", { enum: ServerStatus })
    status: ServerStatus;

    @CreateDateColumn({
        precision: 6,
        comment: 'วันที่สร้างข้อมูล'
    })
    created_at: Date;

    @UpdateDateColumn({
        comment: 'วันที่แก้ไขข้อมูล'
    })
    updated_at: Date;

    @ManyToOne(type => ServerEntity, server => server.id)
    server: ServerEntity;

}
