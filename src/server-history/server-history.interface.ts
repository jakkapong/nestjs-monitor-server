export enum ServerStatus {
    UP = 'up',
    DOWN = 'down',
}