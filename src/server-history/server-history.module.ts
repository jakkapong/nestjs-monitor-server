import { Module } from '@nestjs/common';
import { ServerHistoryService } from './server-history.service';
import { ServerHistoryController } from './server-history.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServerHistoryEntity } from './server-history.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            ServerHistoryEntity,
        ]),
    ],
    providers: [
        ServerHistoryService,
    ],
    controllers: [
        ServerHistoryController,
    ],
    exports: [
        ServerHistoryService
    ]
})
export class ServerHistoryModule { }
