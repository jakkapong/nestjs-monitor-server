import { Test, TestingModule } from '@nestjs/testing';
import { ServerHistoryService } from './server-history.service';

describe('ServerHistoryService', () => {
  let service: ServerHistoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ServerHistoryService],
    }).compile();

    service = module.get<ServerHistoryService>(ServerHistoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
