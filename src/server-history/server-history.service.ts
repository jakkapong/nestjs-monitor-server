import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

import { ServerEntity } from 'src/server/server.entity';
import { ServerHistoryEntity } from './server-history.entity';

import { ServerStatus } from './server-history.interface';

@Injectable()
export class ServerHistoryService {

    constructor(
        @InjectRepository(ServerHistoryEntity)
        private serverHistoryRepository: Repository<ServerHistoryEntity>,
    ) { }

    async create(server: ServerEntity, status: ServerStatus): Promise<ServerHistoryEntity> {
        try {
            const serverHistory = await this.serverHistoryRepository.create({
                status: status,
                server: server
            });
            await this.serverHistoryRepository.save(serverHistory);
            return serverHistory;
        } catch (error) {
            return error;
        }
    }

}
