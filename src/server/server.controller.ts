import { Controller, Get, Query, Post, Put, Body, Delete, Param } from '@nestjs/common';
import { ServerService } from './server.service';
import { ServerDTO } from './server.dto';

@Controller('server')
export class ServerController {

    constructor(
        private readonly serverService: ServerService,
    ) { }

    @Get()
    showAll() {
        return this.serverService.showAll();
    }

    @Post('/unpause/:id')
    unpausePingByServerId(@Param('id') id: string) {
        return this.serverService.unpausePingByServerId(id);
    }

    @Post('/pause/:id')
    pausePingByServerId(@Param('id') id: string) {
        return this.serverService.pausePingByServerId(id);
    }

    @Post()
    createServer(@Body() data: ServerDTO) {
        return this.serverService.create(data);
    }

    @Put('/:id')
    updateServer() {

    }

    @Delete('/:id')
    deleteServer(@Param('id') id: string) {
        return this.serverService.delete(id);
    }
}
