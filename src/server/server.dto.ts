export class ServerDTO {
    name: string;
    ip_address: string;
    port: number;
}