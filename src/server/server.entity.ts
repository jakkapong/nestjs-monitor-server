import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany } from "typeorm";
import { ServerHistoryEntity } from "src/server-history/server-history.entity";

@Entity('server')
export class ServerEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    type: 'varchar',
    length: 200,
    comment: 'ชื่อเซิฟเวอร์'
  })
  name: string;

  @Column({
    type: 'varchar',
    length: 50,
    comment: 'IP Address'
  })
  ip_address: string;

  @Column({
    type: 'integer',
    precision: 2,
    comment: 'Port'
  })
  port: number;

  @Column({
    type: 'tinyint',
    precision: 2,
    comment: 'สถานะ',
    default: 1
  })
  status: number;

  @Column({
    type: 'tinyint',
    precision: 1,
    comment: 'เปิด/ปิด การใช้งาน',
    default: 1
  })
  is_active: boolean;

  @CreateDateColumn({
    precision: 6,
    comment: 'วันที่สร้างข้อมูล'
  })
  created_at: Date;

  @UpdateDateColumn({
    comment: 'วันที่แก้ไขข้อมูล'
  })
  updated_at: Date;

  @OneToMany(type => ServerHistoryEntity, serverHistory => serverHistory.server)
  photos: ServerHistoryEntity[];

}