import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServicesModule } from 'src/services/services.module';

import { ServerService } from './server.service';
import { ServerController } from './server.controller';

import { ServerEntity } from './server.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            ServerEntity,
        ]),
        ServicesModule,
    ],
    providers: [
        ServerService,
    ],
    exports: [
        ServerService
    ],
    controllers: [ServerController]
})
export class ServerModule { }
