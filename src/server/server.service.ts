import { Injectable } from '@nestjs/common';
import { ServerEntity } from './server.entity';
import { Repository, Not } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ServerDTO } from './server.dto';
import { ServicesService } from './../services/services.service';
// import { ServerHistoryService } from 'src/server-history/server-history.service';

@Injectable()
export class ServerService {

    constructor(
        @InjectRepository(ServerEntity)
        private serverRepository: Repository<ServerEntity>,
        // private readonly serverHistoryService: ServerHistoryService,
        private readonly servicesService: ServicesService,
    ) { }

    showAll() {
        // return this.serverRepository.find({
        //     where: {
        //         status: Not(99)
        //     },
        //     order: {
        //         created_at: 'DESC'
        //     }
        // });
        return this.servicesService.showById('580530d9-1dc7-4f94-83db-df3395dbcac0');
    }

    async unpausePingByServerId(id: string) {
        try {
            const server = await this.serverRepository.findOne(id);
            return this.servicesService.pingPauseById(server.id);

        } catch (error) {

        }
    }

    async pausePingByServerId(id: string) {
        try {
            const server = await this.serverRepository.findOne(id);
            return this.servicesService.pingPauseById(server.id);

        } catch (error) {

        }
    }

    // Add new server
    async create(data: ServerDTO) {
        try {
            const server = await this.serverRepository.create(data);
            await this.serverRepository.save(server);
            this.servicesService.pingStartByServer(server);
            return server;
        } catch (error) {
            console.error(error);
            return error;
        }
    }


    async delete(id: string) {
        try {
            await this.serverRepository.update({ id: id }, { status: 99 });
            return 'Deleted successfully';
        } catch (error) {
            console.error(error);
            return error;
        }
    }

    async pingStartById(id: string) {
        try {
            const server = await this.serverRepository.findOne({ where: { id: id } });
            console.log(server);

        } catch (error) {
            console.error(error);
            return error;
        }
    }

    async updateIsActiveById(id: string, isActive: boolean = true): Promise<boolean> {
        try {
            await this.serverRepository.update({ id: id }, { is_active: isActive });
            return true;
        } catch (error) {
            return error;
        }
    }

    async initializeLoadServer(): Promise<ServerEntity[]> {
        return this.serverRepository.find({
            where: {
                status: Not(99),
                is_active: true
            }
        });
    }

}