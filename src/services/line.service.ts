import { Injectable, HttpService, Logger } from "@nestjs/common";

import * as qs from 'querystring';

@Injectable()
export class LineService {

    private readonly urlLineNotify = 'https://notify-api.line.me/api/notify';
    private readonly logger = new Logger('LineService');

    constructor(
        private readonly httpService: HttpService,
    ) { }

    // ส่งข้อความไปยังไลน์
    alertLineMonitorServer(state: string, msg: string, address: string, port: number) {
        const token = process.env.LINE_NOTIFY || '';
        let message = `เซิฟเวอร์ ${msg} ที่อยู่ ${address}:${port} `;
        let stickerPackageId = 0;
        let stickerId = 0;

        if (state === 'up') {
            message += `สถานะการใช้งานปกติ`;
            stickerPackageId = 1;
            stickerId = 407;
        } else {
            message += `สถานะกำลังมีปัญหากรุณาตรวจสอบด่วน!!`;
            stickerPackageId = 1;
            stickerId = 123;
        }
        const headersRequest = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': `Bearer ${token}`,
        };
        this.httpService.post(this.urlLineNotify, qs.stringify({ message }), { headers: headersRequest })
            .toPromise()
            .then(() => {
                return true
            })
            .catch(error => {
                if (error.response) {
                    const err = `${error.response.status} ${error.response.statusText}`;
                    this.logger.error(err, JSON.stringify(error.response.data || ''));
                } else {
                    this.logger.error('Error: send line notify');
                }
                return false;
            })
    }

}