import { Module, HttpModule } from "@nestjs/common";
import { ServerHistoryModule } from "src/server-history/server-history.module";

import { ServicesService } from "./services.service";
import { LineService } from "./line.service";

@Module({
    imports: [
        HttpModule,
        ServerHistoryModule,
    ],
    providers: [
        ServicesService,
        LineService,
    ],
    exports: [
        ServicesService,
        LineService,
    ]
})
export class ServicesModule { }