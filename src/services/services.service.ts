import { Injectable } from '@nestjs/common';

import { ServerEntity } from 'src/server/server.entity';

import { ServerHistoryService } from 'src/server-history/server-history.service';
import { LineService } from './line.service';

import { ServerStatus } from 'src/server-history/server-history.interface';

import * as Monitor from 'ping-monitor';

@Injectable()
export class ServicesService {

    private monitor: any = {};
    private lastMonitor: any = {};

    constructor(
        private readonly serverHistoryService: ServerHistoryService,
        private readonly lineService: LineService,
    ) { }

    showById(id: string) {
        console.log(this.monitor[id]);
        return true;
    }

    async pingStartByServer(server: ServerEntity) {
        this.monitor[server.id] = new Monitor({
            address: server.ip_address,
            title: server.name,
            port: server.port,
            interval: 1 // minutes
        });

        this.monitor[server.id].on('up', async (res, state) => {
            if (this.lastMonitor[server.id] === undefined) {
                // Add server history
                await this.serverHistoryService.create(server, ServerStatus.UP);
                this.lineService.alertLineMonitorServer(ServerStatus.UP, this.monitor[server.id].title, this.monitor[server.id].address, this.monitor[server.id].port);
                this.lastMonitor[server.id] = true;
            }
            if (this.lastMonitor[server.id] === false) {
                // Add server history
                await this.serverHistoryService.create(server, ServerStatus.UP);
                this.lineService.alertLineMonitorServer(ServerStatus.UP, this.monitor[server.id].title, this.monitor[server.id].address, this.monitor[server.id].port);
                this.lastMonitor[server.id] = true;
            }

        });

        this.monitor[server.id].on('down', async (res, state) => {
            console.log('Oh Snap!! ' + res.address + ':' + res.port + ' is down! ');
        });

        this.monitor[server.id].on('stop', async (res, state) => {
            console.log(res.address + ' monitor has stopped.');
        });

        this.monitor[server.id].on('error', async (error) => {
            if (this.lastMonitor[server.id] === undefined) {
                // Add server history
                await this.serverHistoryService.create(server, ServerStatus.DOWN);
                this.lineService.alertLineMonitorServer(ServerStatus.DOWN, this.monitor[server.id].title, this.monitor[server.id].address, this.monitor[server.id].port);
                this.lastMonitor[server.id] = false;
            }
            if (this.lastMonitor[server.id] === true) {
                // Add server history
                await this.serverHistoryService.create(server, ServerStatus.DOWN);
                this.lineService.alertLineMonitorServer(ServerStatus.DOWN, this.monitor[server.id].title, this.monitor[server.id].address, this.monitor[server.id].port);
                this.lastMonitor[server.id] = false;
            }
        });
    }

    pingPauseById(id: string) {
        console.log(this.monitor[id].pause());

        return;
    }

    pingUnPauseById(id: string) {
        return this.monitor[id].unpause();
    }

}
