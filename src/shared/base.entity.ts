import { CreateDateColumn, UpdateDateColumn, DeleteDateColumn, Column } from 'typeorm';
import { Exclude, Transform } from 'class-transformer';
import * as moment from 'moment';

export class BaseEntity {
  @Transform(date => date ? moment(date).format('YYYY-MM-DD HH:mm:ss') : null)
  @DeleteDateColumn({ name: 'deleted_at' })
  @Exclude()
  public deletedAt: Date

  @Transform(date => moment(date).format('YYYY-MM-DD HH:mm:ss'))
  @CreateDateColumn({ name: 'created_at' })
  public createdAt: Date

  @Transform(date => moment(date).format('YYYY-MM-DD HH:mm:ss'))
  @UpdateDateColumn({ name: 'updated_at' })
  public updatedAt: Date

  @Column({
    name: 'created_by',
    type: 'varchar',
    length: 30,
    comment: 'รหัสพนักงาน บันทึกข้อมูล',
  })
  public createdBy: string

  @Column({
    name: 'updated_by',
    type: 'varchar',
    length: 30,
    comment: 'รหัสพนักงาน แก้ไขข้อมูล',
  })
  public updatedBy: string

  @Column({
    name: 'deleted_by',
    type: 'varchar',
    length: 30,
    comment: 'รหัสพนักงาน ลบข้อมูล',
  })
  @Exclude()
  public deletedBy: string
}
