import { Catch, ExceptionFilter, HttpException, ArgumentsHost, Logger, HttpStatus } from '@nestjs/common';
import { HttpErrorFilterReponse, HttpErrorFilterStatus } from './http-error.model';
import * as moment from 'moment'

@Catch()
export class HttpErrorFilter implements ExceptionFilter {

  catch(exception: HttpException, host: ArgumentsHost): any {
    const ctx = host.switchToHttp();
    const request = ctx.getRequest();
    const reponse = ctx.getResponse();
    const status = exception.getStatus ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR;

    // จัดรูปแบบการ Response
    const errorResponse: HttpErrorFilterReponse = {
      status: HttpErrorFilterStatus.FAIL,
      code: status,
      timestamp: moment().format('YYYY-MM-DD HH:mm:ss'),
      path: request.url,
      method: request.method,
      message: (status !== HttpStatus.INTERNAL_SERVER_ERROR) ? (exception.message || null) : 'Internal server error',
      errors: null,
    };

    // format Validator @nestjsx/crud
    if (exception.hasOwnProperty('response')) {
      const response: any = exception.getResponse()
      errorResponse.message = response.hasOwnProperty('error') ? response.error : errorResponse.message
      errorResponse.errors = response.hasOwnProperty('message') ? response.message : errorResponse.errors
    }

    if (status === HttpStatus.INTERNAL_SERVER_ERROR) {
      console.error(exception);
    }

    Logger.error(
      `${request.method} ${request.url}`,
      JSON.stringify(errorResponse),
      'ExceptionFilter'
    );

    reponse.status(status).json(errorResponse);
  }

}