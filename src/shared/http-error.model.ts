export interface HttpErrorFilterReponse {
  status: HttpErrorFilterStatus
  code: number
  timestamp: string
  path: string
  method: string
  message: string
  errors: any
}

export enum HttpErrorFilterStatus {
  SUCCESS = 'success',
  FAIL = 'fail'
}