import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { BaseEntity } from "src/shared/base.entity";

@Entity('user')
export class UserEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column({
    name: 'username',
    type: 'varchar',
    length: 250,
    comment: 'ชื่อ(ผู้ใช้งาน)',
    nullable: false,
    unique: true,
  })
  public username: string

  @Column({
    name: 'password',
    type: 'varchar',
    length: 250,
    comment: 'รหัสผ่าน(ผู้ใช้งาน)',
    nullable: false,
  })
  public password: string
}