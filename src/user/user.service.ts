import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Repository } from 'typeorm';

import { UserEntity } from './user.entity';

@Injectable()
export class UserService extends TypeOrmCrudService<UserEntity> {
  private logger = new Logger('UserService')

  constructor(
    @InjectRepository(UserEntity)
    public repo: Repository<UserEntity>,
  ) {
    super(repo)
  }
}
